<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PetugasModel extends Model
{
    protected $table = 'petugas';

    protected $fillable = [
       'id_petugas','nama_petugas','is_active',
    ];
}
