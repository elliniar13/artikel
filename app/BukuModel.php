<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuModel extends Model
{
    protected $table = 'buku';
   
    public function hasManyPinjam (){

        return $this->hasMany(PinjamModel::class, 'id_buku','id_buku');
    }

    public function hasManyPengembalian (){
        return $this->hasMany(PengembalianModel::class, 'id_buku','id_buku');
    } 

}
