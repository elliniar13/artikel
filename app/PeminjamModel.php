<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PeminjamModel extends Model
{
    protected $table = 'peminjam';

    protected $fillable = [
       'id_peminjam','nama_peminjam','Alamat_peminjam','telp','is_active','created_at','updated_at'
    ];
   
    public function hasManyPinjam (){
        
        return $this->hasMany(PinjamModel::class, 'id_peminjam','id_peminjam');
    }
    public function hasManyPengembalian (){
        
        return $this->hasMany(PengembalianModel::class, 'id_pengembalian','id_pengembalian');
    }
} 
