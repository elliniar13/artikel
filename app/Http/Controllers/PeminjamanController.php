<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\PinjamModel;
use App\BukuModel;
use DB;

class PeminjamanController extends Controller
{
    public function index(){
        $data = DB::table('peminjaman')
        ->join('buku', 'peminjaman.id_buku', '=', 'buku.id_buku')
        ->select('peminjaman.id_peminjaman','peminjaman.tgl_pinjam', 'peminjaman.keterangan','buku.penulis','buku.penerbit','buku.id_buku', 'buku.nama_buku', 'buku.penulis', 'buku.penerbit')
        ->where('peminjaman.is_active', '1')
        ->get();

        return view('admin.layouts.tablepinjam',compact('data'));
    }
    
    public function createData1(){
        return view('admin.pinjam');
    }

    public function posDataPeminjaman(Request $request, PinjamModel $pinjamModel){
        $simpan = $pinjamModel->create([
            'id_buku' => $request->id_buku,
            'tgl_pinjam' => $request->tgl_pinjam,
            'keterangan' => $request->keterangan,
            'is_active' => $request->is_active,
            'created_at' => now(),
            'updated_at' => now(),

        ]);

        if(!$simpan->exists){
            return redirect()->route('tampil_pinjam')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_pinjam')->with('success','data berhasil disimpan');
    }

    public function editData($id){
        $data = PinjamModel::where('id_peminjaman',$id)->first();
        $data2 = BukuModel::all();
        return view('admin.layouts.editpinjam',compact('data','data2'));
    }

    public function updatepinjam($id,PinjamModel $PinjamModel, Request $request){
            $simpan = $PinjamModel->where('id_peminjaman',$id)->update([
                'id_buku' => $request->id_buku,
                'tgl_pinjam' => $request->tgl_pinjam,
                'keterangan' => $request->keterangan,
                'is_active' => $request->is_active,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
    
            if(!$simpan){
                return redirect()->route('tampil_pinjam')->with('error','data gagal di update');
            }
    
            return redirect()->route('tampil_pinjam')->with('success','data berhasil di update');
    }

    public function softdeletepeminjaman($id,PinjamModel $pinjamModel, Request $request){
        $simpan = $pinjamModel->where('id_peminjaman',$id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('tampil_buku')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_buku')->with('success','data berhasil di update');
    }

}