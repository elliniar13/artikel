<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App\BukuModel;
use App\PinjamModel;
use App\PeminjamModel;

class DashboardController extends Controller
{
    public function createDataDashboard(PinjamModel $PinjamModel){
        
        $data= BukuModel::all();
        $data2= PeminjamModel::all();
        return view('uas.tambahdashboard',compact('data','data2'));
        
    }

    public function posDataPeminjaman(Request $request, PinjamModel $pinjamModel){
        $simpan = $pinjamModel->create([
            'id_peminjaman' => $request->id_peminjaman,
            'id_peminjam' => $request->id_peminjam,
            'id_buku' => $request->id_buku,
            'keterangan' => $request->keterangan,
            'is_active' => $request->is_active,
            'tgl_pinjam' => $request->tgl_pinjam,
            'created_at' => now(),

        ]); 

        if(!$simpan->exists){
            return redirect()->route('dashboard')->with('error','data gagal disimpan');
        }

        return redirect()->route('dashboard')->with('success','data berhasil disimpan');
    }

    public function editDataDashboard($id){
        $data = PinjamModel::where('id_peminjaman',$id)->first();
        $data2 = BukuModel::all();
        $data3 = PeminjamModel::all();
        return view('uas.editdashboard',compact('data','data2','data3'));
    }

    public function updateDataDashboard($id,PinjamModel $PinjamModel, Request $request){
            $simpan = $PinjamModel->where('id_peminjaman',$id)->update([
                'id_peminjaman' => $request->id_peminjaman,
                'id_peminjam' => $request->id_peminjam,
                'id_buku' => $request->id_buku,
                'keterangan' => $request->keterangan,
                'is_active' => $request->is_active,
                'tgl_pinjam' => $request->tgl_pinjam,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
    
            if(!$simpan){
                return redirect()->route('dashboard')->with('error','data gagal di update');
            }
    
            return redirect()->route('dashboard')->with('success','data berhasil di update');
    }

    public function softdeleteDashboard($id,PinjamModel $PinjamModel, Request $request){
        $simpan = $PinjamModel->where('id_peminjaman',$id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('dashboard')->with('error','data gagal di update');
        }

        return redirect()->route('dashboard')->with('success','data berhasil di update');
    }
}
