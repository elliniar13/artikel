<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use DB;
use App\BukuModel;
use App\PinjamModel;



class ArtikelController extends Controller
{
// VIEW TAMPIL ALL

    public function index(){
        $data = PinjamModel::all()
        ->where('is_active', '1');

        return view('admin.dashboard', compact('data'));
    }

    public function createData(){
        return view('admin.tambah');
    }

    public function postData(Request $request, BukuModel $bukuModel){
        $simpan = $bukuModel->create([
            'id_buku' => $request->id_buku,
            'nama_buku' => $request->nama_buku,
            'penulis' => $request->penulis,
            'penerbit' => $request->penerbit,
            'is_active' => $request->is_active,

        ]);

        if(!$simpan->exists){
            return redirect()->route('tampil_buku')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_buku')->with('success','data berhasil disimpan');
    }


    public function tampil_buku(){
        $data = DB::table('buku')
        ->where('is_active', '1')
        ->get();
        
        return view('admin.layouts.tables',compact('data'));
    }

    // TAMPIL DASHBOARD 

    public function editData($id){
        $data = BukuModel::where('id_buku',$id)->first();
        return view('admin.layouts.edit',compact('data'));
    }

    public function updateData($id,BukuModel $bukuModel, Request $request){
        $simpan = $bukuModel->where('id_buku',$id)->update([
            'id_buku' => $request->id_buku,
            'nama_buku' => $request->nama_buku,
            'penulis' => $request->penulis,
            'penerbit' => $request->penerbit,
        ]);

        if(!$simpan){
            return redirect()->route('tampil_buku')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_buku')->with('success','data berhasil di update');
    }


    public function softdelete($id,BukuModel $bukuModel, Request $request){
        $simpan = $bukuModel->where('id_buku',$id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('tampil_buku')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_buku')->with('success','data berhasil di update');
    }

    function cariaja(Request $request){
        $cari = $request->cari;

        $data = DB::table('buku')
        ->where('nama_buku', 'like', '%' .$cari. "%")
        ->orwhere('id_buku', 'like', '%' .$cari. "%")
        ->paginate();

        return view('admin.layouts.tables', compact('data'));
    }
   
}