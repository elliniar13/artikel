<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PengembalianModel;
use App\PeminjamModel;
use App\PinjamModel;
use App\BukuModel;


class Pengembaliancontroller extends Controller
{
    public function index (){
        $data= PengembalianModel::all()
        ->where('is_active', '1');

        return view('uas.pengembalian', compact('data'));
    }

    public function createDatapengembalian(PengembalianModel $PengembalianModel){

        $data= PengembalianModel::all();
        $data2= PinjamModel::all();
        $data3= PeminjamModel::all();
        $data1= BukuModel::all();
        return view('uas.tambahpengembalian',compact('data','data2','data3','data1'));
        
    }

    public function posDataPengembalian(Request $request, PengembalianModel $pengembalianModel){
        $simpan = $pengembalianModel->create([
            'id_pengembalian' => $request->id_pengembalian,
            'id_peminjaman' => $request->id_peminjaman,
            'id_peminjam' => $request->id_peminjam,
            'id_buku' => $request->id_buku,
            'nama_buku' => $request->nama_buku,
            'tgl_pengembalian' => $request->tgl_pengembalian,
            'keterangan' => $request->keterangan,
            'is_active' => $request->is_active,
            'tgl_pinjam' => $request->tgl_pinjam,
            'created_at' => now(),

        ]); 

        if(!$simpan->exists){
            return redirect()->route('tampil_pengembalian')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_pengembalian')->with('success','data berhasil disimpan');
    }

    public function editDataPengembalian($id){
        $data = PengembalianModel::where('id_pengembalian',$id)->first();
        $data2 = PinjamModel::all();
        $data3 = PeminjamModel::all();
        $data4 = BukuModel::all();
        return view('uas.editpengembalian',compact('data','data2','data3','data4'));
    }

    public function updateDataPengembalian($id,PengembalianModel $PengembalianModel, Request $request){
            $simpan = $PengembalianModel->where('id_pengembalian',$id)->update([
                'id_pengembalian' => $request->id_pengembalian,
                'id_peminjaman' => $request->id_peminjaman,
                'id_peminjam' => $request->id_peminjam,
                'id_buku' => $request->id_buku,
                'keterangan' => $request->keterangan,
                'is_active' => $request->is_active,
                'tgl_pengembalian' => $request->tgl_pengembalian,
                'created_at' => now(),
                'updated_at' => now(),
            ]);
    
            if(!$simpan){
                return redirect()->route('tampil_pengembalian')->with('error','data gagal di update');
            }
    
            return redirect()->route('tampil_pengembalian')->with('success','data berhasil di update');
    }

    public function softdeletePengembalian($id,PengembalianModel $PengembalianModel, Request $request){
        $simpan = $PengembalianModel->where('id_pengembalian',$id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('tampil_pengembalian')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_pengembalian')->with('success','data berhasil di update');
    }
}
