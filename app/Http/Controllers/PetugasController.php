<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PetugasModel;

class PetugasController extends Controller
{
    public function index (){
        $data = PetugasModel::all()
        ->where('is_active','1');

        return view('uas.petugas', compact('data'));
    }

    public function createPetugas(){

        return view('uas.tambahpetugas');
    } 

    public function tambah_petugas(Request $request, PetugasModel $petugasModel){
        $simpan = $petugasModel->create([
            'id_petugas' => $request->id_petugas,
            'nama_petugas' => $request->nama_petugas,
            'is_active' => 1,

        ]);

        if(!$simpan->exists){
            return redirect()->route('tampil_petugas')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_petugas')->with('success','data berhasil disimpan');
    }

    public function editDataPetugas($id){
        $data = PetugasModel::where('id_petugas',$id)->first();
        return view('uas.editpetugas',compact('data'));
    }

    public function updateDataPetugas($id,PetugasModel $petugasModel, Request $request){
        $simpan = $petugasModel->where('id_petugas',$id)->update([
            'id_petugas' => $request->id_petugas,
            'nama_petugas' => $request->nama_petugas,
        ]);

        if(!$simpan){
            return redirect()->route('tampil_petugas')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_petugas')->with('success','data berhasil di update');
    }

    public function softdeletepetugas($id,PetugasModel $petugasModel, Request $request){
        $simpan = $petugasModel->where('id_petugas',$id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('tampil_petugas')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_petugas')->with('success','data berhasil di update');
    }
}
