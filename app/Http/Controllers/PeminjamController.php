<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Database\Query\Builder;
use App\PeminjamModel;
use DB;

class PeminjamController extends Controller
{
    public function index (){
        $data = PeminjamModel::all()
        ->where('is_active','1');

        return view('uas.peminjam', compact('data'));
    }
    public function createData(){
        return view('uas.tambahpeminjam');
    } 

    public function tambah_peminjam(Request $request, PeminjamModel $peminjamModel){
        $simpan = $peminjamModel->create([
            'id_peminjam' => $request->id_peminjam,
            'nama_peminjam' => $request->nama_peminjam,
            'Alamat_peminjam' => $request->Alamat_peminjam,
            'telp' => $request->telp,
            'is_active' => $request->is_active,
            'created_at' => now(),
            'updated_at' => now(),

        ]);

        if(!$simpan->exists){
            return redirect()->route('tampil_peminjam')->with('error','data gagal disimpan');
        }

        return redirect()->route('tampil_peminjam')->with('success','data berhasil disimpan');
    }
    public function editDataPeminjam($id){
        $data = PeminjamModel::where('id_peminjam',$id)->first();
        return view('uas.editpeminjam',compact('data'));
    }

    public function updateDataPeminjam($id,PeminjamModel $peminjamModel, Request $request){
        $simpan = $peminjamModel->where('id_peminjam',$id)->update([
            'id_peminjam' => $request->id_peminjam,
            'nama_peminjam' => $request->nama_peminjam,
            'Alamat_peminjam' => $request->Alamat_peminjam,
            'telp' => $request->telp,
            'is_active' => $request->is_active,
            'created_at' => now(),
            'updated_at' => now(),

        ]);

        if(!$simpan){
            return redirect()->route('tampil_peminjam')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_peminjam')->with('success','data berhasil di update');
    }

    public function softdeletepeminjam($id,PeminjamModel $peminjamModel, Request $request){
        $simpan = $peminjamModel->where('id_peminjam',$id)->update([
            'is_active' => '0',
        ]);

        if(!$simpan){
            return redirect()->route('tampil_peminjam')->with('error','data gagal di update');
        }

        return redirect()->route('tampil_peminjam')->with('success','data berhasil di update');
    }

}
