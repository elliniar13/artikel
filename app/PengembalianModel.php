<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PengembalianModel extends Model
{
    protected $table = 'pengembalian';

    protected $fillable = [
        'id_pengembalian','id_peminjaman','id_peminjam','id_buku','tgl_pengembalian','keterangan','is_active','created_at','updated_at'
     ];

    public function havePinjam (){
        return $this->belongsTo(PinjamModel::class, 'id_peminjaman','id_peminjaman');
    }

    public function haveBuku (){
        return $this->belongsTo(BukuModel::class, 'id_buku','id_buku');
    }

    public function havePeminjam(){

        return $this->belongsTo(PeminjamModel::class, 'id_peminjam', 'id_peminjam');
    }
} 
