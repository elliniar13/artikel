<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinjamModel extends Model
{
    protected $table = 'peminjaman';
    protected $fillable = [
        'id_peminjaman','id_peminjam','id_buku','tgl_pinjam','keterangan','is_active','created_at','updated_at'
    ];
    

    public function haveBuku(){

        return $this->belongsTo(BukuModel::class, 'id_buku', 'id_buku');
    }
    public function havePeminjam(){

        return $this->belongsTo(PeminjamModel::class, 'id_peminjam', 'id_peminjam');
    }

    public function havePengembalian (){
        return $this->belongsTo(PinjamModel::class, 'id_peminjaman','id_peminjaman');
    }
 
     
}
