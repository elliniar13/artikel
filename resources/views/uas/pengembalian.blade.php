@extends('admin.layouts.master')
@section('content')

<div class="content">
<a href="{{route('tambah_pengembalian')}}" class="btn btn-danger"> Tambah Data</a>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-info text-bold">
                <h4 class="card-title">Pengembalian</h4>
                <p class="card-category"><?php echo "Pada Tanggal : " . date("Y-m-d h:i") . "<br>" ; ?></p>
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead class="text-warning">
                        <th>Id Pengembalian</th>
                        <th>Id Peminjaman</th>
                        <th>Id Peminjam</th>
                        <th>Nama Peminjam</th>
                        <th>Id Buku</th>
                        <th>Nama Buku</th>
                        <th>Tanggal Pengembalian</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr>
                            <td>{{$row->id_pengembalian}}</td>
                            <td>{{$row->id_peminjaman}}</td>
                            <td>{{$row->id_peminjam}}</td>
                            <td>{{$row->havePeminjam->nama_peminjam}}</td>
                            <td>{{$row->id_buku}}</td>
                            <td>{{$row->haveBuku->nama_buku}}</td>
                            <td>{{$row->tgl_pengembalian}}</td>
                            <td>{{$row->keterangan}}</td>
                            <td>
                                <a href="{{ route ('edit_pengembalian', $row->id_pengembalian)}}" class="btn btn-primary">Edit</a>
                                <a href="{{ route ('softdelete_pengembalian', $row->id_pengembalian)}}" class="btn btn-warning">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection