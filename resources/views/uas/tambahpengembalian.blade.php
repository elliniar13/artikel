@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Siapa Saja Yang Pinjam ?</h4>
                <p class="card-category">Silahkan Di isi</p>
            </div>
            <form action="{{route('post_pengembalian')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>id Pengembalian</label>
                        <input type="text" class="form-control" name="id_pengembalian" id="id_pengembalian">
                    </div>
                    <div class="form-group">
                        <label>ID Peminjaman</label>
                        <select type="text" class="form-control" name="id_peminjaman">
                        <option></option>
                        @foreach($data2 as $row)
                            <option value="{{$row->id_peminjaman}}">{{$row->id_peminjaman}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>ID Peminjam</label>
                        <select type="text" class="form-control" name="id_peminjam">
                        <option></option>
                        @foreach($data3 as $row)
                            <option value="{{$row->id_peminjam}}">{{$row->id_peminjam}} - {{$row->nama_peminjam}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>ID Buku</label>
                        <select type="text" class="form-control" name="id_buku">
                        <option></option>
                        @foreach($data1 as $row)
                            <option value="{{$row->id_buku}}">{{$row->id_buku}} - {{$row->nama_buku}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengembalian</label>
                        <input type="date" class="form-control" name="tgl_pengembalian">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" name="keterangan">
                    </div>
                    <div class="form-group">
                        <label>Is Active</label>
                        <select type="text" class="form-control" name="is_active" placeholder="Masukan Keterangan">
                            <option value="0">0</option>
                            <option value="1">1</option>
                        </select>
                    </div>
                    <div>
                        <input class="btn btn-primary" type="button" value="Kembali">
                        <input class="btn btn-warning" type="submit" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection