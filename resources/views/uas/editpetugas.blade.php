@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Edit Data Petugas</h4>
                <p class="card-category">New employees on 15th September, 2016</p>
            </div>
            <form action="{{ route ('update_petugas', $data->id_petugas)}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>Id Petugas</label>
                        <input type="number" class="form-control" name="id_petugas" value="{{$data->id_petugas}}">
                    </div>
                    <div class="form-group">
                        <label>Nama Petugas</label>
                        <input type="text" class="form-control" name="nama_petugas" value="{{$data->nama_petugas}}">
                    </div>
                    <input type="submit" value="Simpan">
            </form>
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection