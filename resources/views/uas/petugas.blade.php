@extends('admin.layouts.master')
@section('content')
<div class="content">
    <a href="{{ route ('tambah_petugas')}}" class="btn btn-danger"> Tambah Data</a>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">TABLE PETUGAS</h4>
                <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead class="text-warning">
                        <th>id petugas</th>
                        <th>nama petugas</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>


                        @foreach($data as $row)
                        <tr>
                            <td>{{$row->id_petugas}}</td>
                            <td>{{$row->nama_petugas}}</td>
                            <td>
                                <a href="{{ route ('edit_petugas', $row->id_petugas)}}" class="btn btn-primary">Edit</a>
                                <a href="{{ route ('softdelete_petugas', $row->id_petugas)}}" class="btn btn-warning">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection