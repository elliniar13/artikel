@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Data Tambah petugas</h4>
                <p class="card-category">Silahkan Di isi</p>
            </div>
            <form action="{{ route ('post_petugas')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>id petugas</label>
                        <input type="number" class="form-control" name="id_petugas">
                    </div>
                    <div class="form-group">
                        <label>nama petugas</label>
                        <input type="text" class="form-control" name="nama_petugas">
                    </div>
                    <div>
                        <input class="btn btn-primary" type="button" value="Kembali">
                        <input class="btn btn-warning" type="submit" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection