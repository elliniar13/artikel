@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Data Tambah Pengunjung</h4>
                <p class="card-category">Silahkan Di isi</p>
            </div>
            <form action="{{ route('post_peminjam') }}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>id peminjam</label>
                        <input type="number" class="form-control" name="id_peminjam">
                    </div>
                    <div class="form-group">
                        <label>nama_peminjam</label>
                        <input type="text" class="form-control" name="nama_peminjam">
                    </div>
                    <div class="form-group">
                        <label>Alamat</label>
                        <input type="text" class="form-control" name="Alamat_peminjam">
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" class="form-control" name="telp">
                    </div>
                    <div class="form-group">
                        <label>Is Active</label>
                        <select type="text" class="form-control" name="is_active" placeholder="Masukan Keterangan">
                            <option value="0">0</option>
                            <option value="1">1</option>
                        </select>
                    </div>
                    <div>
                        <input class="btn btn-primary" type="button" value="Kembali">
                        <input class="btn btn-warning" type="submit" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection