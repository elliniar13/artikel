@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Edit Data Pengembalian</h4>
                <p class="card-category">New employees on 15th September, 2016</p>
            </div>
            <form action="{{ route ('update_pengembalian', $data->id_pengembalian)}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>ID Pengembalian</label>
                        <input type="number" class="form-control" name="id_pengembalian" value="{{$data->id_pengembalian}}"
                            readonly>
                    </div>
                    <div class="form-group">
                        <label>ID Peminjaman</label>
                        <select type="text" class="form-control" name="id_peminjaman">
                        <option>{{$data->id_peminjaman}}</option>
                        @foreach($data2 as $row)
                            <option value="{{$row->id_peminjaman}}">{{$row->id_peminjaman}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>ID Peminjam</label>
                        <select type="text" class="form-control" name="id_peminjam">
                        <option>{{$data->id_peminjam}}</option>
                        @foreach($data3 as $row)
                            <option value="{{$row->id_peminjam}}">{{$row->id_peminjam}} - {{$row->nama_peminjam}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>ID Buku</label>
                        <select type="text" class="form-control" name="id_buku">
                        <option>{{$data->id_buku}}</option>
                        @foreach($data4 as $row)
                            <option value="{{$row->id_buku}}">{{$row->id_buku}} - {{$row->nama_buku}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pengembalian</label>
                        <input type="date" class="form-control" id="tgl_pengembalian" name="tgl_pengembalian"
                            value="{{$data->tgl_pengembalian}}">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <input type="text" class="form-control" name="keterangan" value="{{$data->keterangan}}">
                    </div>
                    <input type="submit" value="Simpan">
            </form>
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection