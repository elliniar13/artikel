@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Edit Data Peminjam</h4>
                <p class="card-category">New employees on 15th September, 2016</p>
            </div>
            <form action="{{ route ('update_peminjam', $data->id_peminjam)}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>Id Peminjam</label>
                        <input type="number" class="form-control" name="id_peminjam" value="{{$data->id_peminjam}}">
                    </div>
                    <div class="form-group">
                        <label>Nama Peminjam</label>
                        <input type="text" class="form-control" name="nama_peminjam" value="{{$data->nama_peminjam}}">
                    </div>
                    <div class="form-group">
                        <label>Alamat Peminjam</label>
                        <input type="text" class="form-control" name="Alamat_peminjam" value="{{$data->Alamat_peminjam}}">
                    </div>
                    <div class="form-group">
                        <label>Telepon</label>
                        <input type="text" class="form-control" name="telp" value="{{$data->telp}}">
                    </div>
                    <input type="submit" value="Simpan">
            </form>
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection