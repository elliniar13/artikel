@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Tambah Data</h4>
                <p class="card-category">Silahkan Di isi</p>
            </div>
            <form action="{{route ('post_buku')}}" method="post">
            @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>Nama Buku</label>
                        <input type="text" class="form-control" name="nama_buku">
                    </div>
                    <div class="form-group">
                        <label>Penulis</label>
                        <input type="text" class="form-control" name="penulis">
                    </div>
                    <div class="form-group">
                        <label>Penerbit</label>
                        <input type="text" class="form-control" name="penerbit">
                    </div>
                    <div class="form-group">
                        <label>is active</label>
                        <input type="text" class="form-control" name="is_active">
                    </div>
                    <div>
                        <input class="btn btn-primary" type="button" value="Kembali">
                        <input class="btn btn-warning" type="submit" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection
