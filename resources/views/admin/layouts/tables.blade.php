@extends('admin.layouts.master')
@section('content')
<div class="content">
    <a href="{{ route ('tambah_buku')}}" class="btn btn-danger"> Tambah Data</a>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
            <form class="navbar-form col-md-3 float-right" method="get" action="{{route('cariaja')}}">
                            <div class="input-group no-border">
                                <input type="text" name="cari" value="" class="form-control text-light" placeholder="Search...">
                                <button type="submit" class="btn btn-white btn-round btn-just-icon">
                  <i class="material-icons">search</i>
                  <div class="ripple-container"></div>
                </button>
            </div>
            </form>
                <h4 class="card-title">TABLE BUKU</h4>
                <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead class="text-warning">
                        <th>ID</th>
                        <th>Nama Buku</th>
                        <th>Penulis</th>
                        <th>Penerbit</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>


                        @foreach($data as $row)
                        <tr>
                            <td>{{$row->id_buku}}</td>
                            <td>{{$row->nama_buku}}</td>
                            <td>{{$row->penulis}}</td>
                            <td>{{$row->penerbit}}</td>
                            <td>
                                <a href="{{ route ('edit_data', $row->id_buku)}}" class="btn btn-primary">Edit</a>
                                <a href="{{ route ('softdelete', $row->id_buku)}}" class="btn btn-warning">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection