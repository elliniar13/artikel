@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Edit Data Buku</h4>
                <p class="card-category">New employees on 15th September, 2016</p>
            </div>
            <form action="{{ route ('update_data', $data->id_buku)}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>ID</label>
                        <input type="number" class="form-control" name="id_buku" value="{{$data->id_buku}}">
                    </div>
                    <div class="form-group">
                        <label>Nama Buku</label>
                        <input type="text" class="form-control" name="nama_buku" value="{{$data->nama_buku}}">
                    </div>
                    <div class="form-group">
                        <label>Penulis</label>
                        <input type="text" class="form-control" name="penulis" value="{{$data->penulis}}">
                    </div>
                    <div class="form-group">
                        <label>Penerbit</label>
                        <input type="text" class="form-control" name="penerbit" value="{{$data->penerbit}}">
                    </div>
                    <input type="submit" value="Simpan">
            </form>
            </tbody>
            </table>
        </div>
    </div>
</div>
@endsection