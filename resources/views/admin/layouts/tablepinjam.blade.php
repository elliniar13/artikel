@extends('admin.layouts.master')
@section('content')
<div class="content">
    <a href="{{route ('tambah_pinjam')}}" class="btn btn-danger ml-4"> Tambah Data</a>
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">TABLE PEMINJAMAN</h4>
                <!-- <p class="card-category">New employees on 15th September, 2016</p> -->
            </div>
            <div class="card-body table-responsive">
                <table class="table table-hover">
                    <thead class="text-warning">
                        <th>ID Peminjaman</th>
                        <th>Tanggal Pinjam</th>
                        <th>ID Buku</th>
                        <th>Nama Buku</th>
                        <th>Penulis</th>
                        <th>Penerbit</th>
                        <th>Keterangan</th>
                        <th>Aksi</th>
                    </thead>
                    <tbody>
                        @foreach($data as $row)
                        <tr>
                            <td>{{$row->id_peminjaman}}</td>
                            <td>{{$row->tgl_pinjam}}</td>
                            <td>{{$row->id_buku}}</td>
                            <td>{{$row->nama_buku}}</td>
                            <td>{{$row->penulis}}</td>
                            <td>{{$row->penerbit}}</td>
                            <td>{{$row->keterangan}}</td>
                            <td>
                                <a href="{{route ( 'update_pinjam',$row->id_peminjaman )}}"
                                    class="btn btn-primary">Edit</a>
                                <a href="{{route ( 'softdelete_peminjaman',$row->id_peminjaman )}}"
                                    class="btn btn-warning">Hapus</a>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection