@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Data Tambah Peminjaman</h4>
                <p class="card-category">Silahkan Di isi</p>
            </div>
            <form action="{{route ('simpan_pinjam')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>ID Buku</label>
                        <select type="text" class="form-control" name="id_buku">
                        <option>{{$data->id_buku}}</option>
                        @foreach($data2 as $row)
                            <option value="{{$row->id_buku}}">{{$row->id_buku}} - {{$row->nama_buku}}</option>
                        @endforeach
                        </select>
                    </div>
                    <div class="form-group">
                        <label>ID Peminjam</label>
                        <input type="number" class="form-control" name="id_peminjam">
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pinjam</label>
                        <input type="date" class="form-control" name="tgl_pinjam">
                    </div>
                    <div class="form-group">
                        <label>Keterangan</label>
                        <select type="text" class="form-control" name="keterangan" placeholder="Masukan Keterangan">
                            <option value="Selesai">Selesai</option>
                            <option value="Belum Selesai">Belum Selesai</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Is Active</label>
                        <select type="text" class="form-control" name="is_active" placeholder="Masukan Keterangan">
                            <option value="0">0</option>
                            <option value="1">1</option>
                        </select>
                    </div>
                    <div>
                        <input class="btn btn-primary" type="button" value="Kembali">
                        <input class="btn btn-warning" type="submit" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
</div>
@endsection