@extends('admin.layouts.master')
@section('content')
<div class="content">
    <div class="col-lg-12 col-md-12">
        <div class="card">
            <div class="card-header card-header-warning">
                <h4 class="card-title">Tambah Tahbahin</h4>
                <p class="card-category">Silahkan Di isi</p>
            </div>
            <form action="{{route ('post_pinjam')}}" method="post">
                @csrf
                <div class="card-body">
                    <div class="form-group">
                        <label>ID Buku</label>
                        <select type="text" class="form-control" name="id_buku" placeholder="Masukan ID Buku">
                            <option>1</option>
                            <option>2</option>
                            <option>3</option>
                        </select>
                    </div>
                    <div class="form-group">
                        <label>Tanggal Pinjam</label>
                        <input type="date" class="form-control" id="tgl_pinjam" name="tgl_pinjam">
                    </div>
                    <div>
                        <input class="btn btn-primary" type="button" value="Kembali">
                        <input class="btn btn-warning" type="submit" value="Simpan">
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection