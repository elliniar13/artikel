<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/



Route::get ('/','AuthController@index')->name('login_form');
Route::post('/sendlogin','AuthController@sendLoginRequest')->name('login_action');

// TAMPIL DATA MASTER
Route::get('/dasboard','ArtikelController@index')->name('dashboard');
Route::get('/cari', 'ArtikelController@cariaja')->name('cariaja');


// ROUTE BUKU

Route::get('artikel/buku','ArtikelController@tampil_buku')->name('tampil_buku');
Route::get('artikel/buku/tambah','ArtikelController@createData')-> name('tambah_buku');
Route::post('artikel/buku/simpan','ArtikelController@postData')->name('post_buku');

Route::get('artikel/buku/edit/{id}','ArtikelController@editData')->name('edit_data');
Route::post('artikel/buku/update/{id}','ArtikelController@updateData')->name('update_data');
Route::get('artikel/buku/softdelete/{id}','ArtikelController@softdelete')->name('softdelete');

// // ROUTE PEMINJAMAN

Route::get('/peminjaman','PeminjamanController@index')->name('tampil_pinjam');
Route::get('cpeminjaman','PeminjamanController@createData1')->name('tambah_pinjam');
Route::post('tpeminjam','PeminjamanController@posDataPeminjaman')->name('simpan_pinjam');
Route::get('upinjam/{id}','PeminjamanController@editData')->name('update_pinjam');
Route::post('spinjam/{id}','PeminjamanController@updatepinjam')->name('u_pinjam');

Route::get('softdelete/{id}','PeminjamanController@softdeletepeminjaman')->name('softdelete_peminjaman');

//  ROUTE PEMINJAM
Route::get('/peminjam','PeminjamController@index')->name('tampil_peminjam');
Route::get('cpeminjam','PeminjamController@createData')->name('tambah_peminjam');
Route::post('tpeminjam','PeminjamController@tambah_peminjam')->name('post_peminjam');

Route::get('pmedit/{id}','PeminjamController@editDataPeminjam')->name('edit_peminjam');
Route::post('pmupdate/{id}','PeminjamController@updateDataPeminjam')->name('update_peminjam');
Route::get('pmsoftdelete/{id}','PeminjamController@softdeletepeminjam')->name('softdelete_peminjam');

// route petugas
Route::get('/petugas','PetugasController@index')->name('tampil_petugas');
Route::get('cpetugas','PetugasController@createPetugas')->name('tambah_petugas');
Route::post('tpetugas','PetugasController@tambah_petugas')->name('post_petugas');

Route::get('pedit/{id}','PetugasController@editDataPetugas')->name('edit_petugas');
Route::post('pupdate/{id}','PetugasController@updateDataPetugas')->name('update_petugas');
Route::get('psoftdelete/{id}','PetugasController@softdeletepetugas')->name('softdelete_petugas');

// Route Dashboard
Route::get('cdashboard','DashboardController@createDataDashboard')->name('tambah_dashboard');
Route::post('tdashboard','DashboardController@posDataPeminjaman')->name('post_dashboard'); 

Route::get('editd/{id}','DashboardController@editDataDashboard')->name('edit_dashboard');
Route::post('updated/{id}','DashboardController@updateDataDashboard')->name('update_dashboard');
Route::get('softdeleted/{id}','DashboardController@softdeleteDashboard')->name('softdelete_dashboard');

//Route pengembalian
Route::get('/pengembalian','Pengembaliancontroller@index')->name('tampil_pengembalian');
Route::get('cpengembalian','Pengembaliancontroller@createDataPengembalian')->name('tambah_pengembalian');
Route::post('tpengembalian','Pengembaliancontroller@posDataPengembalian')->name('post_pengembalian');

Route::get('editp/{id}','Pengembaliancontroller@editDataPengembalian')->name('edit_pengembalian');
Route::post('updatep/{id}','Pengembaliancontroller@updateDataPengembalian')->name('update_pengembalian');
Route::get('softdeletep/{id}','Pengembaliancontroller@softdeletePengembalian')->name('softdelete_pengembalian');