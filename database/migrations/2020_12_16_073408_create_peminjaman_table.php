<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePeminjamanTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('peminjaman', function (Blueprint $table) {
            $table->string('id_peminjaman')->primary();
            $table->string('id_peminjam');
            $table->foreign('id_peminjam')->references('id_peminjam')->on('peminjam');
            $table->string('id_buku');
            $table->foreign('id_buku')->references('id_buku')->on('buku');
            $table->date('tgl_pinjam');
            $table->string('keterangan');
            $table->tinyinteger('is_active')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('peminjaman');
    } 
}
