<?php

use Illuminate\Database\Seeder;

class petugasSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('petugas') -> insert(array(
            array(
                'id_petugas' => '001',
                'nama_petugas' => 'Baper',
                'is_active' => '1',
                'created_at' => now(),
            ),
        ));
    }
}
