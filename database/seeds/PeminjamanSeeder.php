<?php

use Illuminate\Database\Seeder;

class PeminjamanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('peminjaman') -> insert(array(
            array(
                'id_peminjaman' => '921',
                'id_peminjam' => '001',
                'id_buku' => '111',
                'tgl_pinjam' => '2020-05-05',
                'keterangan' => 'Belum Selesai',
                'is_active' => '1',
                'created_at' => now(),
            ),

            array(
                'id_peminjaman' => '922',
                'id_peminjam' => '001',
                'id_buku' => '112',
                'tgl_pinjam' => '2020-01-01',
                'keterangan' => 'Belum Selesai',
                'is_active' => '1',
                'created_at' => now(),
            ),

            array(
                'id_peminjaman' => '923',
                'id_peminjam' => '001',
                'id_buku' => '113',
                'tgl_pinjam' => '2020-01-01',
                'keterangan' => 'Belum Selesai',
                'is_active' => '1',
                'created_at' => now(),
            ),
            array(
                'id_peminjaman' => '924',
                'id_peminjam' => '001',
                'id_buku' => '114',
                'tgl_pinjam' => '2020-01-01',
                'keterangan' => 'Belum Selesai',
                'is_active' => '1',
                'created_at' => now(),
            ),
        ));
    }
}
