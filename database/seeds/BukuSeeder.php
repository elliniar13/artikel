<?php

use Illuminate\Database\Seeder;

class BukuSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('buku') -> insert(array(
            array(
                'id_buku' => '111',
                'nama_buku' => 'Suami ibuku ternyata ayahku',
                'penulis' => 'ALex Kjelberg',
                'penerbit' => 'Indosiar',
                'is_active' => '1',
                'created_at' => now(),
            ),

            array(
                'id_buku' => '112',
                'nama_buku' => 'Ensiklopedia',
                'penulis' => 'gatau',
                'penerbit' => 'dia',
                'is_active' => '1',
                'created_at' => now(),
            ),
            array(
                'id_buku' => '113',
                'nama_buku' => 'Surgaku',
                'penulis' => 'Indah Subaru',
                'penerbit' => 'ILK',
                'is_active' => '1',
                'created_at' => now(),
            ),
            array(
                'id_buku' => '114',
                'nama_buku' => 'Pengantar Tidur',
                'penulis' => 'Hamzah',
                'penerbit' => 'Grasindo',
                'is_active' => '1',
                'created_at' => now(),
            ),
        ));
        
    }
}
