<?php

use Illuminate\Database\Seeder;

class peminjamSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('peminjam') -> insert(array(
            array(
                'id_peminjam' => '001',
                'nama_peminjam' => 'jimin',
                'Alamat_peminjam' => 'Korea',
                'telp' => '82323',
                'is_active' => '1',
                'created_at' => now(),
            )
        ));
    }
}
